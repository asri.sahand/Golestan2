package repository.sql_repo;

import connection.HsqldbConnection;
import model.major.MajorFactory;
import model.major.Major;
import repository.repo_interface.MajorRepo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Sahand on 6/5/2017.
 */
public class MajorSqlRepo implements MajorRepo{
    private static MajorSqlRepo ourInstance = new MajorSqlRepo();

    public static MajorSqlRepo getInstance() {
        return ourInstance;
    }

    private MajorSqlRepo() {}

    public void addSubMajor(Major subMajor) {

    }

    public Major removeSubMajor(int id) {
        return null;
    }

    public Major findSubMajor(int id) {
        String sql = String.format("SELECT * FROM majors WHERE id = %d", id);
        return getMajorInfo(sql);
    }

    private Major getMajorInfo(String sql) {
        Major major = null;

        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();

            major = MajorFactory.getInstance().createFrom(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return major;
    }
}
