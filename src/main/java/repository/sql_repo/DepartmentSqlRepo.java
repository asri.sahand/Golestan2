package repository.sql_repo;

import connection.HsqldbConnection;
import model.department.DepartmentFactory;
import model.department.Department;
import repository.repo_interface.DepartmentRepo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Sahand on 6/5/2017.
 */
public class DepartmentSqlRepo implements DepartmentRepo {
    private static DepartmentSqlRepo ourInstance = new DepartmentSqlRepo();

    public static DepartmentSqlRepo getInstance() {
        return ourInstance;
    }

    private DepartmentSqlRepo() {}

    public void addDepartment(Department department) {}

    public void removeDepartment(int id) {}

    public Department findDepartment(int id) {
        String sql = String.format("SELECT * FROM departments " +
                "WHERE id = %d", id);
        return getDepartment(sql);
    }

    private Department getDepartment(String sql) {
        Department department = null;
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();

            department = DepartmentFactory
                    .getInstance().createFrom(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return department;
    }
}
