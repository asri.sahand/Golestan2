package repository.sql_repo;

import connection.HsqldbConnection;
import model.professor.ProfessorFactory;
import model.professor.Professor;
import repository.repo_interface.ProfessorRepo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Sahand on 6/6/2017.
 */
public class ProfessorSqlRepo implements ProfessorRepo {
    private static ProfessorSqlRepo ourInstance = new ProfessorSqlRepo();

    public static ProfessorSqlRepo getInstance() {
        return ourInstance;
    }

    private ProfessorSqlRepo() {}

    public void addProfessor(Professor professor) {}

    public void removeProfessor(int id) {}

    public Professor findProfessor(int id) {
        String sql = String.format("SELECT * FROM professors WHERE id = %d", id);
        return getProfessor(sql);
    }

    private Professor getProfessor(String sql) {
        Professor professor = null;

        Connection connection = HsqldbConnection.getConnection();
        Statement statement;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();

            professor = ProfessorFactory
                    .getInstance().createFrom(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return professor;
    }

}
