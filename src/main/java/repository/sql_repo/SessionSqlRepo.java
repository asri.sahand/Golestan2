package repository.sql_repo;

import connection.HsqldbConnection;
import model.session.Session;
import model.session.SessionFactory;
import repository.repo_interface.SessionRepo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class SessionSqlRepo implements SessionRepo {
    private static SessionSqlRepo ourInstance = new SessionSqlRepo();

    public static SessionSqlRepo getInstance() {
        return ourInstance;
    }

    private SessionSqlRepo() {}

    public void addSession(Session session) {}

    public void removeSession(int id) {}

    public Session findSession(int id) {
        return findSessions(id).get(0);
    }

    public List<Session> findSessions(int id) {
        String sql = String.format("SELECT * FROM sessions WHERE id = %d", id);
        return getSessions(sql);
    }

    private List<Session> getSessions(String sql) {
        List<Session> sessions = new ArrayList();

        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Session session = SessionFactory
                        .getInstance().createFrom(resultSet);
                sessions.add(session);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sessions;
    }
}
