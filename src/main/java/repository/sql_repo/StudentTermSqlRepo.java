package repository.sql_repo;

import connection.HsqldbConnection;
import model.student.StudentTermFactory;
import model.student.StudentTerm;
import repository.repo_interface.StudentTermRepo;
import repository.specification.student_term.StudentTermSpec;
import repository.specification.student_term.StudentTermSqlSpec;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class StudentTermSqlRepo implements StudentTermRepo {
    private static StudentTermSqlRepo ourInstance = new StudentTermSqlRepo();

    public static StudentTermSqlRepo getInstance() { return ourInstance; }

    private StudentTermSqlRepo() {}

    public void addStudentTerm(StudentTerm studentTerm) {}

    public void removeStudentTerm(int id) {}

    public StudentTerm findStudentTerm(StudentTermSpec specification) {
        return findStudentTerms(specification).get(0);
    }

    public List<StudentTerm> findStudentTerms(StudentTermSpec specification) {

        StudentTermSqlSpec sqlSpec = (StudentTermSqlSpec) specification;

        String sql = String.format("SELECT * FROM student_terms WHERE %s",
                sqlSpec.toSqlClauses());
        return getStudentTerms(sql);
    }

    private List<StudentTerm> getStudentTerms(String sql) {
        List<StudentTerm> studentTerms = new ArrayList<StudentTerm>();
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while(resultSet.next()) {
                StudentTerm studentTerm = StudentTermFactory
                        .getInstance().createFrom(resultSet);
                studentTerms.add(studentTerm);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentTerms;
    }
}
