package repository.sql_repo;

import connection.HsqldbConnection;
import model.course.CourseOfferFactory;
import model.course.CourseOffer;
import repository.repo_interface.CourseOfferRepo;
import repository.specification.course_offer.CourseOfferSpec;
import repository.specification.course_offer.CourseOfferSqlSpec;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseOfferSqlRepo implements CourseOfferRepo {
    private static CourseOfferSqlRepo ourInstance = new CourseOfferSqlRepo();

    public static CourseOfferSqlRepo getInstance() {
        return ourInstance;
    }

    private CourseOfferSqlRepo() {}

    public void addCourseOffer(CourseOffer courseOffer) {}

    public void updateCourseOffer(CourseOffer courseOffer) {
        String sql = String.format("UPDATE course_offers SET registered_students=%d " +
                        "WHERE id=%d",
                courseOffer.getCourseOfferInfo().getRegisteredStudents(), courseOffer.getId());
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeCourseOffer(CourseOfferSpec specification) {}

    public CourseOffer findCourseOffer(CourseOfferSpec specification) {
        return findCourseOffers(specification).get(0);
    }

    public List<CourseOffer> findCourseOffers(CourseOfferSpec specification) {

        CourseOfferSqlSpec sqlSpec = (CourseOfferSqlSpec) specification;
        String sql = String.format("SELECT * FROM course_offers %s",
                sqlSpec.toSqlClauses());
        return getCourseOffers(sql);

    }

    public List<CourseOffer> findCourseOffers() {
        String sql = "SELECT * FROM course_offers";
        return getCourseOffers(sql);
    }

    private List<CourseOffer> getCourseOffers(String sql) {
        List courseOffers = new ArrayList();
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CourseOffer courseOffer = CourseOfferFactory
                        .getInstance().createFrom(resultSet);
                courseOffers.add(courseOffer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courseOffers;
    }

}
