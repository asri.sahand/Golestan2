package repository.sql_repo;

import connection.HsqldbConnection;
import model.student.TakenCourseFactory;
import model.student.StudentTerm;
import model.student.TakenCourse;
import repository.repo_interface.TakenCourseRepo;
import repository.specification.taken_course.TakenCourseSpec;
import repository.specification.taken_course.TakenCourseSqlSpec;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/5/2017.
 */
public class TakenCourseSqlRepo implements TakenCourseRepo {
    private static TakenCourseSqlRepo ourInstance = new TakenCourseSqlRepo();

    public static TakenCourseSqlRepo getInstance() {
        return ourInstance;
    }

    private TakenCourseSqlRepo() {}

    public void addTakenCourse(TakenCourse takenCourse, StudentTerm studentTerm) {
        int studentTermId = studentTerm.getId();
        int courseOfferId = takenCourse.getCourseOffer().getId();

        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        String sql = String.format("INSERT INTO taken_courses " +
                "(student_term_id, course_offer_id) VALUES (%d, %d)",
                studentTermId, courseOfferId);
        try {
            statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeTakenCourse(TakenCourseSpec specification) {

        TakenCourseSqlSpec sqlSpec = (TakenCourseSqlSpec) specification;
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        String sql = String.format("DELETE FROM taken_courses WHERE %s",
                sqlSpec.toSqlClauses());
        try {
            statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public TakenCourse findTakenCourse(TakenCourseSpec specification) {
        return null;
    }

    public List<TakenCourse> findTakenCourses(TakenCourseSpec specification) {

        TakenCourseSqlSpec sqlSpec = (TakenCourseSqlSpec) specification;
        String sql = String.format("SELECT * FROM taken_courses WHERE %s",
                sqlSpec.toSqlClauses());
        return getTakenCourses(sql);
    }

    private List<TakenCourse> getTakenCourses(String sql) {
        List<TakenCourse> takenCourses = new ArrayList<TakenCourse>();
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                TakenCourse takenCourse =
                        TakenCourseFactory.getInstance().createFrom(resultSet);
                takenCourses.add(takenCourse);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return takenCourses;
    }
}
