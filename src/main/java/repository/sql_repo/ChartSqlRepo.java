package repository.sql_repo;

import connection.HsqldbConnection;
import model.chart.Chart;
import model.chart.ChartFactory;
import model.major.SubMajor;
import repository.repo_interface.ChartRepo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Sahand on 7/1/2017.
 */
public class ChartSqlRepo implements ChartRepo{
    private static ChartSqlRepo ourInstance = new ChartSqlRepo();

    public static ChartSqlRepo getInstance() {
        return ourInstance;
    }

    private ChartSqlRepo() {}


    public void addChart(Chart chart) {

    }

    public void updateChart(Chart chart) {

    }

    public void removeChart(Chart chart) {

    }

    public Chart findChart(SubMajor subMajor, int year) {
        String sql = String.format("SELECT * FROM charts " +
                "WHERE sub_major_id=%d AND year=%d", subMajor.getId(), year);
        return getChart(sql);
    }

    private Chart getChart(String sql) {
        Chart chart = null;
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            chart = ChartFactory
                    .getInstance().createFrom(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chart;
    }
}
