package repository.sql_repo;

import connection.HsqldbConnection;
import model.course.CourseFactory;
import model.course.Course;
import repository.repo_interface.CourseRepo;
import repository.specification.course.CourseSpec;
import repository.specification.course.CourseSqlSpec;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseSqlRepo implements CourseRepo {
    private static CourseSqlRepo ourInstance = new CourseSqlRepo();

    public static CourseSqlRepo getInstance() {
        return ourInstance;
    }

    private CourseSqlRepo() {}

    public void addCourse(Course course) {

    }

    public void removeCourse(Course course) {

    }

    public Course findCourse(CourseSpec specification) {
        return findCourses(specification).get(0);
    }

    public List<Course> findCourses(CourseSpec specification) {
        CourseSqlSpec sqlSpec = (CourseSqlSpec) specification;

        String sql = String.format("SELECT * FROM courses " +
                "WHERE %s", sqlSpec.toSqlClauses());
        return getCourses(sql);
    }

    private List<Course> getCourses(String sql) {
        List courses = new ArrayList();

        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Course course = CourseFactory
                        .getInstance().createFrom(resultSet);
                courses.add(course);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courses;
    }
}
