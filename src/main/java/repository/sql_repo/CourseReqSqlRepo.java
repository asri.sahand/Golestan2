package repository.sql_repo;

import connection.HsqldbConnection;
import model.chart.CourseReq;
import model.chart.CourseReqFactory;
import repository.repo_interface.CourseReqRepo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 7/1/2017.
 */
public class CourseReqSqlRepo implements CourseReqRepo {
    private static CourseReqSqlRepo ourInstance = new CourseReqSqlRepo();

    public static CourseReqSqlRepo getInstance() {
        return ourInstance;
    }

    private CourseReqSqlRepo() {}

    public void addCourseReq(CourseReq courseReq) {

    }

    public void removeCourseReq(CourseReq courseReq) {

    }

    public List<CourseReq> findCourseReqs(int chartId) {
        String sql = String.format("SELECT * FROM course_reqs " +
                "WHERE chart_id = %d", chartId);
        return getCourseReqs(sql);
    }

    private List<CourseReq> getCourseReqs(String sql) {
        List courses = new ArrayList();

        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CourseReq courseReq = CourseReqFactory
                        .getInstance().createFrom(resultSet);
                courses.add(courseReq);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courses;
    }
}
