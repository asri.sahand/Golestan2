package repository.sql_repo;

import connection.HsqldbConnection;
import model.term.Term;
import model.term.TermFactory;
import repository.repo_interface.TermRepo;
import repository.specification.term.TermSpec;
import repository.specification.term.TermSqlSpec;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class TermSqlRepo implements TermRepo {
    private static TermSqlRepo ourInstance = new TermSqlRepo();

    public static TermSqlRepo getInstance() {
        return ourInstance;
    }

    private TermSqlRepo() {
    }

    public void addTerm(Term term) {

    }

    public void removeTerm(TermSpec specification) {}

    public Term findTerm(TermSpec specification) {

        TermSqlSpec sqlSpec = (TermSqlSpec) specification;

        String sql = String.format("SELECT * FROM terms WHERE %s",
                sqlSpec.toSqlClauses());
        return getTerm(sql);
    }

    public List<Term> findTerms() {
        String sql = "SELECT * FROM terms";
        return getTerms(sql);
    }

    private Term getTerm(String sql) {
        return getTerms(sql).get(0);
//        Term term = null;
//        Connection connection = HsqldbConnection.getConnection();
//        Statement statement = null;
//        try {
//            statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery(sql);
//            resultSet.next();
//
//            int id = resultSet.getInt("id");
//            int year = resultSet.getInt("year");
//            int num = resultSet.getInt("num");
//
//            term = new Term(id, year, num);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return term;
    }

    private List<Term> getTerms(String sql) {
        List<Term> terms = new ArrayList<Term>();
        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Term term = TermFactory.getInstance().createFrom(resultSet);
                terms.add(term);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return terms;
    }
}
