package repository.sql_repo;

import connection.HsqldbConnection;
import model.major.SubMajor;
import model.major.SubMajorFactory;
import repository.repo_interface.SubMajorRepo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Sahand on 6/5/2017.
 */
public class SubMajorSqlRepo implements SubMajorRepo {
    private static SubMajorSqlRepo ourInstance = new SubMajorSqlRepo();

    public static SubMajorSqlRepo getInstance() {
        return ourInstance;
    }

    private SubMajorSqlRepo() {
    }

    public void addSubMajor(SubMajor subMajor) {

    }

    public SubMajor removeSubMajor(int id) {
        return null;
    }

    public SubMajor findSubMajor(int id) {

        String sql = String.format("SELECT * FROM sub_majors WHERE id = %d", id);
        return getSubMajorInfo(sql);
    }

    private SubMajor getSubMajorInfo(String sql) {
        SubMajor subMajor = null;

        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();

            subMajor = SubMajorFactory.getInstance().createFrom(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subMajor;
    }
}
