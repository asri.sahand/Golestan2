package repository.sql_repo;

import connection.HsqldbConnection;
import model.student.StudentFactory;
import model.student.Student;
import repository.repo_interface.StudentRepo;
import repository.specification.student.StudentSpec;
import repository.specification.student.StudentSqlSpec;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Sahand on 5/21/2017.
 */
public class StudentSqlRepo implements StudentRepo {
    private static StudentSqlRepo ourInstance = new StudentSqlRepo();

    public static StudentSqlRepo getInstance() {
        return ourInstance;
    }

    private StudentSqlRepo() {
    }

    public void addStudent(Student student) {
        String firstName = student.getPersonIdentity().getFirstName();
        String lastName = student.getPersonIdentity().getLastName();
        int gender = student.getPersonIdentity().getGender();
        String sid = student.getStudentIdentity().getSid();
        int type = student.getStudentIdentity().getType();
        int enteringYear = student.getStudentIdentity().getEnteringYear();

        Connection connection = HsqldbConnection.getConnection();
        String query = "INSERT INTO students (first_name, last_name, gender, " +
                "sid, student_type, entering_year) VALUES ('" +  firstName +
                "', '" + lastName + "', " + gender + ", '" + sid + "', " +
                type + ", " +  enteringYear + ")";
        // System.out.println(query);
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateStudent(String oldSid, Student student) {
    }

    public void removeStudent(Student student) {}

    public Student findStudent(StudentSpec studentSpec) {
        return getStudentInfo(studentSpec);
    }

    private Student getStudentInfo(StudentSpec studentSpec) {
        Student student = null;
        StudentSqlSpec sqlSpec = (StudentSqlSpec) studentSpec;

        Connection connection = HsqldbConnection.getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    String.format("SELECT * FROM students WHERE %S",
                            sqlSpec.toSqlClauses()));
            while (resultSet.next()) {
                student = StudentFactory.getInstance().createFrom(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }

}
