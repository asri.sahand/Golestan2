package repository.repo_interface;

import model.professor.Professor;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface ProfessorRepo {
    public void addProfessor(Professor professor);
    public void removeProfessor(int id);
    public Professor findProfessor(int id);
}
