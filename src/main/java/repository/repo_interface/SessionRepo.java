package repository.repo_interface;

import model.session.Session;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface SessionRepo {
    public void addSession(Session session);
    public void removeSession(int id);
    public Session findSession(int id);
    public List<Session> findSessions(int id);
}
