package repository.repo_interface;

import model.term.Term;
import repository.specification.term.TermSpec;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface TermRepo {
    public void addTerm(Term term);
    public void removeTerm(TermSpec specification);
    public Term findTerm(TermSpec specification);
    public List<Term> findTerms();
}
