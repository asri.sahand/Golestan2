package repository.repo_interface;

import model.major.SubMajor;

/**
 * Created by Sahand on 6/5/2017.
 */
public interface SubMajorRepo {
    public void addSubMajor(SubMajor subMajor);
    public SubMajor removeSubMajor(int id);
    public SubMajor findSubMajor(int id);
}
