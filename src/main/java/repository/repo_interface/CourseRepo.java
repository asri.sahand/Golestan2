package repository.repo_interface;

import model.course.Course;
import repository.specification.course.CourseSpec;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface CourseRepo {

    public void addCourse(Course course);
    public void removeCourse(Course course);
    public Course findCourse(CourseSpec specification);
    public List<Course> findCourses(CourseSpec specification);

}
