package repository.repo_interface;


import model.chart.Chart;
import model.major.SubMajor;

/**
 * Created by Sahand on 7/1/2017.
 */
public interface ChartRepo {
    public void addChart(Chart chart);
    public void updateChart(Chart chart);
    public void removeChart(Chart chart);
    public Chart findChart(SubMajor subMajor, int year);
}
