package repository.repo_interface;

import model.chart.CourseReq;

import java.util.List;

/**
 * Created by Sahand on 7/1/2017.
 */
public interface CourseReqRepo {
    public void addCourseReq(CourseReq courseReq);
    public void removeCourseReq(CourseReq courseReq);
    public List<CourseReq> findCourseReqs(int chartId);
}
