package repository.repo_interface;

import model.department.Department;

/**
 * Created by Sahand on 6/5/2017.
 */
public interface DepartmentRepo {

    public void addDepartment(Department department);
    public void removeDepartment(int id);
    public Department findDepartment(int id);
}
