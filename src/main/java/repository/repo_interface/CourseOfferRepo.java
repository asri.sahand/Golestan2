package repository.repo_interface;

import model.course.CourseOffer;
import repository.specification.course_offer.CourseOfferSpec;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface CourseOfferRepo {
    public void addCourseOffer(CourseOffer courseOffer);
    public void updateCourseOffer(CourseOffer courseOffer);
    public void removeCourseOffer(CourseOfferSpec specification);
    public CourseOffer findCourseOffer(CourseOfferSpec specification);
    public List<CourseOffer> findCourseOffers(CourseOfferSpec specification);
    public List<CourseOffer> findCourseOffers();
}
