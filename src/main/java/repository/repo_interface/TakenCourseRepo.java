package repository.repo_interface;

import model.student.StudentTerm;
import model.student.TakenCourse;
import repository.specification.taken_course.TakenCourseSpec;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface TakenCourseRepo {
    public void addTakenCourse(TakenCourse takenCourse, StudentTerm studentTerm);
    public void removeTakenCourse(TakenCourseSpec specification);
    public TakenCourse findTakenCourse(TakenCourseSpec specification);
    public List<TakenCourse> findTakenCourses(TakenCourseSpec specification);
}
