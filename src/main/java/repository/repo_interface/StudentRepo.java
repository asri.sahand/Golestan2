package repository.repo_interface;

import model.student.Student;
import repository.specification.student.StudentSpec;

/**
 * Created by Sahand on 6/5/2017.
 */
public interface StudentRepo {
    public void addStudent(Student student);
    public void updateStudent(String oldSid, Student student);
    public void removeStudent(Student student);

    public Student findStudent(StudentSpec specification);
}
