package repository.repo_interface;

import model.student.StudentTerm;
import repository.specification.student_term.StudentTermSpec;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface StudentTermRepo {
    public void addStudentTerm(StudentTerm studentTerm);
    public void removeStudentTerm(int id);
    public StudentTerm findStudentTerm(StudentTermSpec specification);
    public List<StudentTerm> findStudentTerms(StudentTermSpec specification);
}
