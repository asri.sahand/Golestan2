package repository.repo_interface;

import model.major.Major;

/**
 * Created by Sahand on 6/5/2017.
 */
public interface MajorRepo {
    public void addSubMajor(Major major);
    public Major removeSubMajor(int id);
    public Major findSubMajor(int id);
}
