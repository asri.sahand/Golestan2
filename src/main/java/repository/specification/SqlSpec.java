package repository.specification;

/**
 * Created by Sahand on 6/2/2017.
 */
public interface SqlSpec {
    String toSqlClauses();
}
