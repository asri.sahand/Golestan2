package repository.specification.student;

import model.student.Student;

/**
 * Created by Sahand on 6/2/2017.
 */
public class StudentSqlSpecByEYear implements StudentSqlSpec {
    private int enteringYear;

    public StudentSqlSpecByEYear(int enteringYear) {
        this.enteringYear = enteringYear;
    }

    public String toSqlClauses() {
        return String.format("entering_year = %d", enteringYear);
    }

    public boolean specified(Student student) {
        return false;
    }
}
