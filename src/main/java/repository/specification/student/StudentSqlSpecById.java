package repository.specification.student;

import model.student.Student;

/**
 * Created by Sahand on 6/5/2017.
 */
public class StudentSqlSpecById implements StudentSqlSpec {
    private String sid;

    public StudentSqlSpecById(String sid) {
        this.sid = sid;
    }

    public String toSqlClauses() {
        return String.format("sid = '%s'", sid);
    }

    public boolean specified(Student student) {
        return false;
    }
}
