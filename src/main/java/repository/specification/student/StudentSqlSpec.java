package repository.specification.student;

import repository.specification.SqlSpec;

/**
 * Created by Sahand on 6/5/2017.
 */
public interface StudentSqlSpec extends SqlSpec, StudentSpec {}
