package repository.specification.student;

import model.student.Student;

/**
 * Created by Sahand on 6/2/2017.
 */
public interface StudentSpec {
    boolean specified(Student student);
}
