package repository.specification.term;

import repository.specification.SqlSpec;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface TermSqlSpec extends SqlSpec, TermSpec {}
