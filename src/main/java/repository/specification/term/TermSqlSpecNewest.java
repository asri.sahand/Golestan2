package repository.specification.term;

/**
 * Created by Sahand on 6/6/2017.
 */
public class TermSqlSpecNewest implements TermSqlSpec {

    public String toSqlClauses() {
        return "ID IN (SELECT MAX(ID) FROM TERMS)";
    }
}
