package repository.specification.term;

/**
 * Created by Sahand on 6/6/2017.
 */
public class TermSqlSpecById implements TermSqlSpec {

    private int id;

    public TermSqlSpecById(int id) {
        this.id = id;
    }

    public String toSqlClauses() {
        return String.format("id = %d", id);
    }
}
