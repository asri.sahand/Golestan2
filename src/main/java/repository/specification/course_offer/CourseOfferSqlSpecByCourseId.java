package repository.specification.course_offer;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseOfferSqlSpecByCourseId implements CourseOfferSqlSpec {

    private int courseId;

    public CourseOfferSqlSpecByCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String toSqlClauses() {
        return String.format("WHERE course_id = %d", courseId);
    }
}
