package repository.specification.course_offer;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseOfferSqlSpecById implements CourseOfferSqlSpec {
    private int id;

    public CourseOfferSqlSpecById(int id) {
        this.id = id;
    }

    public String toSqlClauses() {
        return String.format("WHERE id = %d", id);
    }
}
