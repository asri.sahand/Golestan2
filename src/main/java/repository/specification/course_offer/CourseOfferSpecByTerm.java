package repository.specification.course_offer;

import model.term.Term;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseOfferSpecByTerm implements CourseOfferSqlSpec {

    private Term term;

    public CourseOfferSpecByTerm(Term term) {
        this.term = term;
    }

    public String toSqlClauses() {
        return String.format("JOIN terms ON " +
                "course_offers.term_id = terms.id " +
                "WHERE year = %d AND num = %d", term.getYear(), term.getNum());
    }
}
