package repository.specification.course_offer;

import repository.specification.SqlSpec;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface CourseOfferSqlSpec extends SqlSpec, CourseOfferSpec {}
