package repository.specification.student_term;

import repository.specification.SqlSpec;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface StudentTermSqlSpec extends SqlSpec, StudentTermSpec {}
