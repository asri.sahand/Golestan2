package repository.specification.student_term;

/**
 * Created by Sahand on 6/6/2017.
 */
public class StudentTermSqlSpecBySid implements StudentTermSqlSpec {

    private String sid;

    public StudentTermSqlSpecBySid(String sid) {
        this.sid = sid;
    }

    public String toSqlClauses() {
        return String.format("student_sid = '%s'", sid);
    }
}
