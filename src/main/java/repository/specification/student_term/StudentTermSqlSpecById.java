package repository.specification.student_term;

import model.student.Student;

/**
 * Created by Sahand on 6/6/2017.
 */
public class StudentTermSqlSpecById implements StudentTermSqlSpec {

    private int id;

    public StudentTermSqlSpecById(int id) {
        this.id = id;
    }

    public String toSqlClauses() {
        return String.format("id = %d", id);
    }

    public boolean specified(Student student) {
        return false;
    }
}
