package repository.specification.student_term;

/**
 * Created by Sahand on 6/6/2017.
 */
public class StudentTermSpecBySidAndTermId implements StudentTermSqlSpec {

    private String sid;
    private int tid;

    public StudentTermSpecBySidAndTermId(String sid, int tid) {
        this.sid = sid;
        this.tid = tid;
    }

    public String toSqlClauses() {
        return String.format("student_sid = %s AND term_id = %d", sid, tid);
    }
}
