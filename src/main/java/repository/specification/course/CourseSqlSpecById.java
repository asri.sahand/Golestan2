package repository.specification.course;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseSqlSpecById implements CourseSqlSpec {

    private int id;

    public CourseSqlSpecById(int id) {
        this.id = id;
    }

    public String toSqlClauses() {
        return String.format("id = %d", id);
    }
}
