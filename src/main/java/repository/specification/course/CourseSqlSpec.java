package repository.specification.course;

import repository.specification.SqlSpec;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface CourseSqlSpec extends SqlSpec, CourseSpec {}
