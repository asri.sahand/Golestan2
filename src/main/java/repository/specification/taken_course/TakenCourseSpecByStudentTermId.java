package repository.specification.taken_course;

/**
 * Created by Sahand on 6/6/2017.
 */
public class TakenCourseSpecByStudentTermId implements TakenCourseSqlSpec{

    private int studentTermId;

    public TakenCourseSpecByStudentTermId(int studentTermId) {
        this.studentTermId = studentTermId;
    }

    public String toSqlClauses() {
        return String.format("student_term_id = %d", studentTermId);
    }
}
