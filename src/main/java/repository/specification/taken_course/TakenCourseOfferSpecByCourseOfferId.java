package repository.specification.taken_course;

/**
 * Created by Sahand on 6/6/2017.
 */
public class TakenCourseOfferSpecByCourseOfferId implements TakenCourseSqlSpec{

    private int courseOfferId;

    public TakenCourseOfferSpecByCourseOfferId(int courseOfferId) {
        this.courseOfferId = courseOfferId;
    }

    public String toSqlClauses() {
        return String.format("course_offer_id = %d", courseOfferId);
    }
}
