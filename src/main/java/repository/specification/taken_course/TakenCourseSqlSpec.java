package repository.specification.taken_course;

import repository.specification.SqlSpec;

/**
 * Created by Sahand on 6/6/2017.
 */
public interface TakenCourseSqlSpec extends SqlSpec, TakenCourseSpec {}
