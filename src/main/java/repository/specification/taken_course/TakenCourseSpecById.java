package repository.specification.taken_course;

/**
 * Created by Sahand on 6/6/2017.
 */
public class TakenCourseSpecById implements TakenCourseSqlSpec{

    private int id;

    public TakenCourseSpecById(int id) {
        this.id = id;
    }

    public String toSqlClauses() {
        return String.format("id = %d", id);
    }
}
