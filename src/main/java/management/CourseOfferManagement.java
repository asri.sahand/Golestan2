package management;

import model.course.CourseOffer;
import model.session.Session;

/**
 * Created by Sahand on 7/1/2017.
 */
public class CourseOfferManagement {
    private static CourseOfferManagement ourInstance = new CourseOfferManagement();

    public static CourseOfferManagement getInstance() {
        return ourInstance;
    }

    private CourseOfferManagement() {}

    public void showCourseOfferInfo(CourseOffer courseOffer) {
        System.out.println("ID: " + courseOffer.getId());
        System.out.println(courseOffer.getCourse().getName()
                + "(" + courseOffer.getCourse().getUnit() + ")");
        System.out.println(courseOffer.getCourseOfferInfo().getGenderString());
        System.out.println(courseOffer.getCourseOfferInfo().getRegisteredStudents()
                + " Of " + courseOffer.getCourseOfferInfo().getCapacity());
        System.out.println(courseOffer.getProfessor().getLastName());
        System.out.println(courseOffer.getTerm().getName());
        System.out.println(courseOffer.getCourse().getDepartment().getName());
        for (Session session: courseOffer.getSessions())
            System.out.println("\t" + session.getDay() + " / " + session.getTime());
        System.out.println("************************************************************************");
    }
}
