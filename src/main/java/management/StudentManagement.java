package management;

import model.student.Student;
import model.student.StudentTerm;
import model.student.TakenCourse;
import repository.specification.student.StudentSqlSpecById;
import repository.specification.taken_course.TakenCourseSpecByStudentTermId;
import repository.sql_repo.StudentSqlRepo;
import repository.sql_repo.TakenCourseSqlRepo;

/**
 * Created by Sahand on 7/1/2017.
 */
public class StudentManagement {
    private static StudentManagement ourInstance = new StudentManagement();

    public static StudentManagement getInstance() {
        return ourInstance;
    }

    private StudentManagement() {}

    public void showStudentInfo(Student student) {
        System.out.println(student.getPersonIdentity().getFirstName());
        System.out.println(student.getStudentIdentity().getEnteringYear());
        System.out.println(student.getSubMajor().getName());
        System.out.println(student.getSubMajor().getMajor().getName());
        System.out.println(student.getSubMajor().getMajor().getDepartment().getName());
        System.out.println("************************************************************************");
        int i = 1;
        for (StudentTerm studentTerm : student.getStudentTerms()) {
            System.out.print("\t"
//                    + studentTerm.getId() + ". "
                    + i + ". "
                    + studentTerm.getTerm().getName() + " -> "
                    + studentTerm.getGpa());
            System.out.println(String.format(", taken courses: %d",
                    studentTerm.getTakenCourses().size()));
            i++;
        }
        i = 1;
        System.out.println("************************************************************************");
        for (StudentTerm studentTerm : student.getStudentTerms()) {
            System.out.print(i + ". Term" + studentTerm.getTerm().getName() + ": ");
            for (TakenCourse takenCourse: studentTerm.getTakenCourses()) {
                System.out.print(takenCourse.getCourseOffer()
                        .getCourse().getName() + ", ");
            }
            System.out.println();
            i++;
        }
        System.out.println("************************************************************************");
    }

    public Student reloadStudent(String sid) {
        return StudentSqlRepo.getInstance()
                .findStudent(new StudentSqlSpecById(sid));
    }

    public void reloadStudentTakenCourses(Student student) {
        for (StudentTerm studentTerm: student.getStudentTerms()) {
            int studentTermId = studentTerm.getId();
            studentTerm.setTakenCourses(TakenCourseSqlRepo.getInstance()
                    .findTakenCourses(new TakenCourseSpecByStudentTermId(studentTermId)));
        }
    }
}
