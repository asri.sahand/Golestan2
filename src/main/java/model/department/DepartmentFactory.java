package model.department;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class DepartmentFactory {
    private static DepartmentFactory ourInstance = new DepartmentFactory();

    public static DepartmentFactory getInstance() {
        return ourInstance;
    }

    private DepartmentFactory() {}

    public Department createWith(int id, String name) {
        return new Department(id, name);
    }

    public Department createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        return new Department(id, name);
    }
}
