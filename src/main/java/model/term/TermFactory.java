package model.term;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class TermFactory {
    private static TermFactory ourInstance = new TermFactory();

    public static TermFactory getInstance() {
        return ourInstance;
    }

    private TermFactory() {}

    public Term createWith(int year, int num) {
        return new Term(year, num);
    }

    public Term createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int year = resultSet.getInt("year");
        int num = resultSet.getInt("num");

        return new Term(id, year, num);
    }
}
