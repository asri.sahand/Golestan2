package model.term;

/**
 * Created by Sahand on 6/6/2017.
 */
public class Term {
    private int id;
    private int year;
    private int num;
    private String name;

    Term(int id, int year, int num) {
        this.id = id;
        this.year = year;
        this.num = num;
        this.name = year + "-" + num;
    }

    Term(int year, int num) {
        this.year = year;
        this.num = num;
        this.name = year + "-" + num;
    }

    public int getId() {
        return id;
    }

    public int getYear() {
        return year;
    }

    public int getNum() {
        return num;
    }

    public String getName() {
        return name;
    }

    public Term getPrevTerm() {
        if (this.num > 1) {
            return new Term(this.year, num-1);
        } else {
            return new Term(this.year, 2);
        }
    }
}
