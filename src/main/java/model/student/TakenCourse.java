package model.student;

import model.course.CourseOffer;
import repository.specification.course_offer.CourseOfferSqlSpecById;
import repository.sql_repo.CourseOfferSqlRepo;

/**
 * Created by Sahand on 6/5/2017.
 */
public class TakenCourse {
    private int id;
    private CourseOffer courseOffer;
    private double grade;

    TakenCourse(CourseOffer courseOffer) {
        this.courseOffer = courseOffer;
    }

    TakenCourse(CourseOffer courseOffer, double grade) {
        this.courseOffer = courseOffer;
        this.grade = grade;
    }

    TakenCourse(int id, CourseOffer courseOffer, double grade) {
        this.id = id;
        this.courseOffer = courseOffer;
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setCourseOffer(CourseOffer courseOffer) { this.courseOffer = courseOffer ;}

    public CourseOffer getCourseOffer() {
        if (this.courseOffer.getCourse() == null) {
            CourseOffer courseOffer = CourseOfferSqlRepo
                    .getInstance().findCourseOffer(
                            new CourseOfferSqlSpecById(
                                    this.courseOffer.getId()));
            this.courseOffer = courseOffer;
        }
        return this.courseOffer;
    }

//    public void loadCourseOffer() {
//        CourseOffer courseOffer = CourseOfferSqlRepo
//                .getInstance().findCourseOffer(
//                        new CourseOfferSqlSpecById(
//                                this.courseOffer.getId()));
//        this.courseOffer = courseOffer;
//    }

    public double getGrade() {
        return grade;
    }

    public boolean isPassed() { return this.grade >= 10; }
}
