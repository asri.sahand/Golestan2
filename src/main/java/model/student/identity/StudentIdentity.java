package model.student.identity;

import model.major.SubMajor;

/**
 * Created by Sahand on 6/5/2017.
 */
public class StudentIdentity {
    private String sid;
    private int type;
    private int enteringYear;

    public StudentIdentity(String sid, int type, int enteringYear) {
        this.sid = sid;
        this.type = type;
        this.enteringYear = enteringYear;
    }

    public String getSid() {
        return sid;
    }

    public int getType() {
        return type;
    }

    public int getEnteringYear() {
        return enteringYear;
    }
}
