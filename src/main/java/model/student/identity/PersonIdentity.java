package model.student.identity;

/**
 * Created by Sahand on 6/5/2017.
 */
public class PersonIdentity {
    private String firstName;
    private String lastName;
    private int gender;

    public PersonIdentity(String firstName, String lastName, int gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getGender() {
        return gender;
    }
}
