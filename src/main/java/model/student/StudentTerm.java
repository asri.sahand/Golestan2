package model.student;

import model.term.Term;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class StudentTerm {
    private int id;
    private Term term;
    private double gpa;
    private List<TakenCourse> takenCourses;

    StudentTerm(Term term, double gpa, List<TakenCourse> takenCourses) {
        this.term = term;
        this.gpa = gpa;
        this.takenCourses = takenCourses;
    }

    StudentTerm(int id, Term term, double gpa, List<TakenCourse> takenCourses) {
        this.id = id;
        this.term = term;
        this.gpa = gpa;
        this.takenCourses = takenCourses;
//        this.takenCourses = new ArrayList<TakenCourse>();
    }

    public int getId() {
        return id;
    }

    public Term getTerm() {
        return term;
    }

    public double getGpa() {
        return gpa;
    }

    public List<TakenCourse> getTakenCourses() {
        return takenCourses;
    }

    public void setTakenCourses(List<TakenCourse> takenCourses) {
        this.takenCourses = takenCourses;
    }

    public boolean isTerm(Term term) {
        return this.term.getId() == term.getId();
    }
}
