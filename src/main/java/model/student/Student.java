package model.student;

import model.major.SubMajor;
import model.student.identity.PersonIdentity;
import model.student.identity.StudentIdentity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/5/2017.
 */
public class Student {
    public static final int NORMAL_STUDENT = 1;
    public static final int GUEST_STUDENT = 2;
    public static final int MALE = 1;
    public static final int FEMALE = 2;
    public static final int BOTH_MALE_AND_FEMALE = 3;

    private PersonIdentity personIdentity;
    private StudentIdentity studentIdentity;
    private SubMajor subMajor;
    private List<StudentTerm> studentTerms;
    private List<TakenCourse> takenCourses;

    protected Student(PersonIdentity pIdentity, StudentIdentity sIdentity) {
        this.personIdentity = pIdentity;
        this.studentIdentity = sIdentity;
        this.takenCourses = new ArrayList<TakenCourse>();
        this.studentTerms = new ArrayList<StudentTerm>();
    }

    public void setSubMajor(SubMajor subMajor) {
        this.subMajor = subMajor;
    }

    public void setStudentTerms(List<StudentTerm> studentTerms) {
        this.studentTerms = studentTerms;
    }

    public void addToTakenCourse(TakenCourse takenCourse) {
        this.takenCourses.add(takenCourse);
    }

    public boolean isMale() {
        return this.personIdentity.getGender() == MALE;
    }

    public boolean isFemale() {
        return this.personIdentity.getGender() == FEMALE;
    }

    public PersonIdentity getPersonIdentity() {
        return personIdentity;
    }

    public StudentIdentity getStudentIdentity() {
        return studentIdentity;
    }

    public SubMajor getSubMajor() {
        return subMajor;
    }

    public List<TakenCourse> getTakenCourses() {
        return takenCourses;
    }

    public List<StudentTerm> getStudentTerms() {
        return studentTerms;
    }

    public void addStudentTerm(StudentTerm studentTerm) {
        this.studentTerms.add(studentTerm);
    }

    public static class IrregularStudentTypeException extends Exception {
        public IrregularStudentTypeException() {}

        public IrregularStudentTypeException(String message)
        {
            super(message);
        }
    }

}
