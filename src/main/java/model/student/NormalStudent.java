package model.student;

import model.student.identity.PersonIdentity;
import model.student.identity.StudentIdentity;

/**
 * Created by Sahand on 6/5/2017.
 */
public class NormalStudent extends Student {

    public NormalStudent(PersonIdentity pIdentity,
                         StudentIdentity sIdentity) {

        super(pIdentity, sIdentity);
    }
}
