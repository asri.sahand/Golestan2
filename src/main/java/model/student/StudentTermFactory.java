package model.student;

import model.term.Term;
import repository.specification.taken_course.TakenCourseSpecByStudentTermId;
import repository.specification.term.TermSqlSpecById;
import repository.sql_repo.TakenCourseSqlRepo;
import repository.sql_repo.TermSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Sahand on 6/30/2017.
 */
public class StudentTermFactory {
    private static StudentTermFactory ourInstance = new StudentTermFactory();

    public static StudentTermFactory getInstance() {
        return ourInstance;
    }

    private StudentTermFactory() {}

    public StudentTerm createWith(Term term
            , List<TakenCourse> takenCourses, double gpa) {
        return new StudentTerm(term, gpa, takenCourses);
    }

    public StudentTerm createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int termId = resultSet.getInt("term_id");
        double gpa = resultSet.getDouble("gpa");

        //TODO CHECK
        Term term = TermSqlRepo.getInstance().findTerm(
                new TermSqlSpecById(id));
        List<TakenCourse> takenCourses = TakenCourseSqlRepo.getInstance()
                .findTakenCourses(new TakenCourseSpecByStudentTermId(id));

        return new StudentTerm(id, term, gpa, takenCourses);
    }
}
