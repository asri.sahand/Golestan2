package model.student;

import model.major.SubMajor;
import model.student.identity.PersonIdentity;
import model.student.identity.StudentIdentity;
import repository.specification.student_term.StudentTermSqlSpecBySid;
import repository.sql_repo.StudentTermSqlRepo;
import repository.sql_repo.SubMajorSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class StudentFactory {
    private static StudentFactory ourInstance = new StudentFactory();

    public static StudentFactory getInstance() {
        return ourInstance;
    }

    private StudentFactory() {}

    public Student createWith(PersonIdentity pIdentity, StudentIdentity sIdentity) {
        if (sIdentity.getType() == Student.NORMAL_STUDENT) {
            return new NormalStudent(pIdentity, sIdentity);
        } else if(sIdentity.getType() == Student.GUEST_STUDENT) {
            return new GuestStudent(pIdentity, sIdentity);
        } else {
            new Student.IrregularStudentTypeException();
            return null;
        }
    }
    public Student createFrom(ResultSet resultSet) throws SQLException {
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String sid = resultSet.getString("sid");
            int gender = resultSet.getInt("gender");
            int type = resultSet.getInt("student_type");
            int enteringYear = resultSet.getInt("entering_year");
            int subMajorId = resultSet.getInt("sub_major_id");

            PersonIdentity personIdentity =
                    new PersonIdentity(firstName, lastName, gender);
            StudentIdentity studentIdentity =
                    new StudentIdentity(sid, type, enteringYear);

            SubMajor subMajor = SubMajorSqlRepo.
                    getInstance().findSubMajor(subMajorId);
            Student student = createWith(personIdentity, studentIdentity);
            student.setSubMajor(subMajor);
            student.setStudentTerms(StudentTermSqlRepo.getInstance()
                    .findStudentTerms(new StudentTermSqlSpecBySid(sid)));
        return student;
    }
}
