package model.student;

import model.course.CourseOffer;
import model.course.CourseOfferFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class TakenCourseFactory {
    private static TakenCourseFactory ourInstance = new TakenCourseFactory();

    public static TakenCourseFactory getInstance() {
        return ourInstance;
    }

    private TakenCourseFactory() {}

    public TakenCourse createWith(CourseOffer courseOffer) {
        return new TakenCourse(courseOffer);
    }

    public TakenCourse createWith(CourseOffer courseOffer, double grade) {
        return new TakenCourse(courseOffer, grade);
    }

    public TakenCourse createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int courseOfferId = resultSet.getInt("course_offer_id");
        double grade = resultSet.getDouble("grade");

        CourseOffer courseOffer =
                CourseOfferFactory.getInstance().createWith(courseOfferId);

        return new TakenCourse(id, courseOffer, grade);
    }
}
