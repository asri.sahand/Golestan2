package model.student;

import model.major.SubMajor;
import model.student.identity.PersonIdentity;
import model.student.identity.StudentIdentity;

/**
 * Created by Sahand on 6/5/2017.
 */
public class GuestStudent extends Student {
    public GuestStudent(PersonIdentity pIdentity,
                        StudentIdentity sIdentity) {

        super(pIdentity, sIdentity);
    }
}
