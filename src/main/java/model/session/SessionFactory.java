package model.session;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class SessionFactory {
    private static SessionFactory ourInstance = new SessionFactory();

    public static SessionFactory getInstance() {
        return ourInstance;
    }

    private SessionFactory() {}

    public Session createWith(String day, double startTime, double endTime) {
        return new Session(day, startTime, endTime);
    }

    public Session createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String day = resultSet.getString("session_day");
        double startTime = resultSet.getDouble("start_time");
        double endTime = resultSet.getDouble("end_time");

        return new Session(id, day, startTime, endTime);
    }
}
