package model.session;

/**
 * Created by Sahand on 6/6/2017.
 */
public class Session {
    private int id;
    private String day;
    private String time;
    private double startTime;
    private double endTime;

    Session(String day, double startTime, double endTime) {
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    Session(int id, String day, double startTime, double endTime) {
        this.id = id;
        this.day = day;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getId() {
        return id;
    }

    public String getDay() {
        return day;
    }

    public double getStartTime() {
        return startTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public String getTime() {
        return String.format("%.1f - %.1f", startTime, endTime);
    }
}
