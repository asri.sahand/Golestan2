package model.chart;

import model.course.Course;
import repository.specification.course.CourseSqlSpecById;
import repository.sql_repo.CourseSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 7/1/2017.
 */
public class CourseReqFactory {
    private static CourseReqFactory ourInstance = new CourseReqFactory();

    public static CourseReqFactory getInstance() {
        return ourInstance;
    }

    private CourseReqFactory() {}

    private CourseReq createWith(Course course, Course reqCourse, int reqType) {
        return new CourseReq(course, reqCourse, reqType);
    }

    public CourseReq createFrom(ResultSet resultSet) throws SQLException {
        int courseId = resultSet.getInt("course_id");
        int reqCourseId = resultSet.getInt("req_course_id");
        int reqType = resultSet.getInt("req_type");

        Course course = CourseSqlRepo.getInstance()
                .findCourse(new CourseSqlSpecById(courseId));
        Course reqCourse = CourseSqlRepo.getInstance()
                .findCourse(new CourseSqlSpecById(reqCourseId));

        return new CourseReq(course, reqCourse, reqType);
    }
}
