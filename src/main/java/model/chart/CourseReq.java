package model.chart;

import model.course.Course;

/**
 * Created by Sahand on 7/1/2017.
 */
public class CourseReq {

    public static final int PRE_REQ = 1;
    public static final int CUR_REQ = 2;

    private Course course;
    private Course reqCourse;
    private int type;

    public CourseReq(Course course, Course reqCourse, int type) {
        this.course = course;
        this.reqCourse = reqCourse;
        this.type = type;
    }

    public Course getCourse() {
        return course;
    }

    public Course getReqCourse() {
        return reqCourse;
    }

    public int getType() {
        return type;
    }

    public void showReqCourse() {
        System.out.println(String.format("%s: %s",
                this.course.getName(), this.reqCourse.getName()));
    }

    public boolean isPreReq() {
        return this.type == PRE_REQ;
    }
}
