package model.chart;

import model.major.SubMajor;
import repository.sql_repo.CourseReqSqlRepo;
import repository.sql_repo.SubMajorSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Sahand on 7/1/2017.
 */
public class ChartFactory {
    private static ChartFactory ourInstance = new ChartFactory();

    public static ChartFactory getInstance() {
        return ourInstance;
    }

    private ChartFactory() {}

    private Chart createWith(int id, SubMajor subMajor,
                             int year, List<CourseReq> courseReqs) {
        return new Chart(id, subMajor, year, courseReqs);
    }

    public Chart createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int subMajorId = resultSet.getInt("sub_major_id");
        int year = resultSet.getInt("year");

        SubMajor subMajor = SubMajorSqlRepo
                .getInstance().findSubMajor(subMajorId);

        List<CourseReq> courseReqs = CourseReqSqlRepo.getInstance()
                .findCourseReqs(id);

        return new Chart(id, subMajor, year, courseReqs);
    }
}
