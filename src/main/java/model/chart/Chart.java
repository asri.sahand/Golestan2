package model.chart;

import model.major.SubMajor;

import java.util.List;

/**
 * Created by Sahand on 7/1/2017.
 */
public class Chart {
    private int id;
    private SubMajor subMajor;
    private int year;
    private List<CourseReq> courseReqs;

    public Chart(int id, SubMajor subMajor, int year, List<CourseReq> courseReqs) {
        this.id = id;
        this.subMajor = subMajor;
        this.year = year;
        this.courseReqs = courseReqs;
    }

    public SubMajor getSubMajor() {
        return subMajor;
    }

    public List<CourseReq> getCourseReqs() {
        return courseReqs;
    }

    public int getId() {
        return id;
    }

    public int getYear() {
        return year;
    }
}
