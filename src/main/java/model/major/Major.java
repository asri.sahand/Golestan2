package model.major;

import model.department.Department;

/**
 * Created by Sahand on 6/5/2017.
 */
public class Major {

    private int id;
    private String name;
    private Department department;

    Major(String name, Department department) {
        this.name = name;
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Department getDepartment() {
        return department;
    }
}
