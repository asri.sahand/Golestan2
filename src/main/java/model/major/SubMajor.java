package model.major;

/**
 * Created by Sahand on 5/21/2017.
 */
public class SubMajor {

    private int id;
    private Major major;
    private String name;

    SubMajor(int id, Major major, String name) {
        this.id = id;
        this.major = major;
        this.name = name;
    }

    SubMajor(Major major, String name) {
        this.major = major;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Major getMajor() {
        return major;
    }

    public String getName() {
        return name;
    }
}
