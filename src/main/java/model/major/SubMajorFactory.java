package model.major;

import repository.sql_repo.MajorSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class SubMajorFactory {
    private static SubMajorFactory ourInstance = new SubMajorFactory();

    public static SubMajorFactory getInstance() {
        return ourInstance;
    }

    private SubMajorFactory() {}

    public SubMajor createWith(Major major, String name) {
        return new SubMajor(major, name);
    }

    public SubMajor createFrom(ResultSet resultSet) throws SQLException {
        int subMajorId = resultSet.getInt("id");
        int majorId = resultSet.getInt("major_id");
        String subMajorName = resultSet.getString("name");

        Major major = MajorSqlRepo.getInstance().findSubMajor(majorId);
        return new SubMajor(subMajorId, major, subMajorName);
    }
}
