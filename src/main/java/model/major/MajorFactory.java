package model.major;

import model.department.Department;
import repository.sql_repo.DepartmentSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class MajorFactory {
    private static MajorFactory ourInstance = new MajorFactory();

    public static MajorFactory getInstance() {
        return ourInstance;
    }

    private MajorFactory() {}

    public Major createWith(int id, int did, String name, Department department) {
        return new Major(name, department);
    }

    public Major createFrom(ResultSet resultSet) throws SQLException {
        int majorId = resultSet.getInt("id");
        int departmentId = resultSet.getInt("did");
        String majorName = resultSet.getString("name");

        Department department = DepartmentSqlRepo
                .getInstance().findDepartment(departmentId);
        return new Major(majorName, department);
    }
}
