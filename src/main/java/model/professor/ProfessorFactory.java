package model.professor;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class ProfessorFactory {
    private static ProfessorFactory ourInstance = new ProfessorFactory();

    public static ProfessorFactory getInstance() {
        return ourInstance;
    }

    private ProfessorFactory() {}

    public Professor createWith(String firstName, String lastName) {
        return new Professor(firstName, lastName);
    }

    public Professor createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");

        return new Professor(id, firstName, lastName);
    }
}
