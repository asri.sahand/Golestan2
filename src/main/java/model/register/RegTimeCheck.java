package model.register;

import model.course.CourseOffer;
import model.session.Session;
import model.student.Student;
import model.student.StudentTerm;
import model.student.TakenCourse;
import repository.specification.course_offer.CourseOfferSqlSpecById;
import repository.sql_repo.CourseOfferSqlRepo;

import java.util.List;

/**
 * Created by Sahand on 6/7/2017.
 */
public class RegTimeCheck implements RegisterCheck {
    private CourseOffer courseOffer;
    private Student student;

    public RegTimeCheck(Student student, CourseOffer courseOffer) {
        this.student = student;
        this.courseOffer = courseOffer;
    }

    public boolean check() {
        List<Session> sessions1 = courseOffer.getSessions();
        StudentTerm lastStudentTerm = student.getStudentTerms()
                .get(student.getStudentTerms().size()-1);
        List<TakenCourse> takenCourses = lastStudentTerm.getTakenCourses();
        for (TakenCourse takenCourse: takenCourses) {
            CourseOffer courseOffer = CourseOfferSqlRepo
                    .getInstance().findCourseOffer(new
                            CourseOfferSqlSpecById(
                            takenCourse.getCourseOffer().getId()));
            for (Session session1: sessions1) {
                for (Session session2 : courseOffer.getSessions()) {
                    if (session1.getDay().equals(session2.getDay())) {
                        if (session1.getStartTime() <= session2.getStartTime() &&
                                session1.getEndTime() >= session2.getStartTime())
                            return false;
                        if (session2.getStartTime() <= session1.getStartTime() &&
                                session2.getEndTime() >= session1.getStartTime())
                            return false;
                    }
                }
            }
        }
        return true;
    }
}
