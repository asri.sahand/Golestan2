package model.register;

import model.chart.Chart;
import model.chart.CourseReq;
import model.course.Course;
import model.course.CourseOffer;
import model.student.Student;
import model.student.StudentTerm;
import model.student.TakenCourse;
import model.term.Term;
import repository.specification.student_term.StudentTermSqlSpecBySid;
import repository.specification.term.TermSqlSpecNewest;
import repository.sql_repo.ChartSqlRepo;
import repository.sql_repo.StudentTermSqlRepo;
import repository.sql_repo.TermSqlRepo;

import java.util.List;

/**
 * Created by Sahand on 7/1/2017.
 */
public class RegReqCheck implements RegisterCheck {

    private Student student;
    private CourseOffer courseOffer;
    private List<StudentTerm> prevStudentTerms;

    public RegReqCheck(Student student, CourseOffer courseOffer) {
        this.student = student;
        this.courseOffer = courseOffer;
    }

    public boolean check() {
        Chart chart = ChartSqlRepo.getInstance().findChart(student.getSubMajor(),
                        student.getStudentIdentity().getEnteringYear());
        CourseReq courseReq = null;
        Course reqCourse = null;

        for (CourseReq courseReq1: chart.getCourseReqs())
            if (courseReq1.getCourse().isCourse(courseOffer.getCourse())) {
                reqCourse = courseReq1.getReqCourse();
                courseReq = courseReq1;
                courseReq1.showReqCourse();
            }

        /*   Ther is no req course for selected course   */
        if (reqCourse == null)
            return true;

        List<StudentTerm> studentTerms = StudentTermSqlRepo.getInstance()
                .findStudentTerms(new StudentTermSqlSpecBySid(
                        student.getStudentIdentity().getSid()));
        if (courseReq.isPreReq()) {
            Term curTerm = TermSqlRepo.getInstance()
                    .findTerm(new TermSqlSpecNewest());
            for (StudentTerm studentTerm: studentTerms)
                if (studentTerm.isTerm(curTerm)) {
                    studentTerms.remove(studentTerm);
                    break;
                }
        }
        for (StudentTerm studentTerm: studentTerms) {
            List<TakenCourse> takenCourses = studentTerm.getTakenCourses();
            for (TakenCourse takenCourse: takenCourses) {
                if (takenCourse.getCourseOffer().isCourse(reqCourse) &&
                        takenCourse.isPassed())
                    return true;
            }
        }
        return false;
    }
}
