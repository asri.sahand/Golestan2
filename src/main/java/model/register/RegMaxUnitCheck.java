package model.register;

import model.course.CourseOffer;
import model.student.Student;
import model.student.StudentTerm;
import model.student.TakenCourse;
import model.term.Term;
import repository.specification.course_offer.CourseOfferSqlSpecById;
import repository.specification.student_term.StudentTermSpecBySidAndTermId;
import repository.specification.student_term.StudentTermSqlSpecById;
import repository.specification.student_term.StudentTermSqlSpecBySid;
import repository.sql_repo.CourseOfferSqlRepo;
import repository.sql_repo.StudentTermSqlRepo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/7/2017.
 */
public class RegMaxUnitCheck implements RegisterCheck {

    private static final int CONDITIONAL_GPA = 12;
    private static final int MAX_UNITS_FOR_CONDITIONAL_STUDENTS = 14;
    private static final int MAX_UNITS_FOR_OTHER_STUDENTS = 3;

    private Student student;
    private StudentTerm prevStudentTerm;
    private StudentTerm curStudentTerm;

    public RegMaxUnitCheck(Student student, Term term) {

        this.student = student;
        this.curStudentTerm =  StudentTermSqlRepo
                .getInstance().findStudentTerm(
                        new StudentTermSpecBySidAndTermId(student
                                .getStudentIdentity().getSid(), term.getId()));
        setPrevStudentTerm();
    }

    public boolean check() {
        int takenUnits = 0;
        for (TakenCourse takenCourse: curStudentTerm.getTakenCourses()) {
            CourseOffer courseOffer = CourseOfferSqlRepo
                    .getInstance().findCourseOffer(
                            new CourseOfferSqlSpecById(takenCourse
                                    .getCourseOffer().getId()));
            takenUnits += courseOffer.getCourse().getUnit();
        }
        System.out.println("takenUnits: " + takenUnits);
        if (prevStudentTerm.getGpa() <= CONDITIONAL_GPA
                && takenUnits <= MAX_UNITS_FOR_CONDITIONAL_STUDENTS) {
            return true;
        } else if(prevStudentTerm.getGpa() >= CONDITIONAL_GPA
                && takenUnits <= MAX_UNITS_FOR_OTHER_STUDENTS) {
            return true;
        } else {
            return false;
        }
    }

    public void setPrevStudentTerm() {
        List<StudentTerm> studentTerms = StudentTermSqlRepo.getInstance()
                .findStudentTerms(new StudentTermSqlSpecBySid(student
                        .getStudentIdentity().getSid()));
        List<StudentTerm> studentTermList = new ArrayList<StudentTerm>();
        for (StudentTerm t: studentTerms)
            studentTermList.add(t);

        int curTermIndex = 0;
        for (int i = 0; i < studentTermList.size(); i++)
            if (studentTermList.get(i).getId() == curStudentTerm.getId())
                curTermIndex = i;
        studentTermList.remove(curTermIndex);

        int prevStudentTermId = 0;
        for (StudentTerm ts: studentTermList) {
            if (prevStudentTermId <= ts.getId())
                prevStudentTermId = ts.getId();
        }
        this.prevStudentTerm = StudentTermSqlRepo.getInstance()
                .findStudentTerm(new StudentTermSqlSpecById(prevStudentTermId));
    }
}
