package model.register;

import model.course.CourseOffer;

/**
 * Created by Sahand on 6/7/2017.
 */
public class RegCapacityCheck implements RegisterCheck {

    private CourseOffer courseOffer;

    public RegCapacityCheck(CourseOffer courseOffer) {
        this.courseOffer = courseOffer;
    }

    public boolean check() {
        return courseOffer.getCourseOfferInfo().getCapacity() >=
                courseOffer.getCourseOfferInfo().getRegisteredStudents() + 1;
    }

//    public void check() throws RegCapacityException {
//        if (courseOffer.getCourseOfferInfo().getCapacity() >=
//                courseOffer.getCourseOfferInfo().getRegisteredStudents() + 1)
//            throw new RegCapacityException();
//    }
//    public class RegCapacityException extends RegisterException {}
}
