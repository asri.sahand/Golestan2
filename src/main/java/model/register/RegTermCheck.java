package model.register;

import model.course.CourseOffer;
import model.term.Term;

/**
 * Created by Sahand on 6/7/2017.
 */
public class RegTermCheck implements RegisterCheck {

    private CourseOffer courseOffer;
    private Term term;

    public RegTermCheck(CourseOffer courseOffer, Term term) {
        this.courseOffer = courseOffer;
        this.term = term;
    }

    public boolean check() {
        return courseOffer.getTerm().getId() == term.getId();
    }

//    public void check() throws RegTermException {
//        if (courseOffer.getTerm().getId() != term.getId())
//            throw new RegTermException();
//    }

//    public class RegTermException extends RegisterException {}
}
