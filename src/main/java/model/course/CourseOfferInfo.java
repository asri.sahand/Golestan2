package model.course;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseOfferInfo {

    private static final int FOR_MALE = 1;
    private static final int FOR_FEMALE = 2;
    private static final int FOR_BOTH = 3;

    private int group;
    private int capacity;
    private int registeredStudents;
    private int gender;

    public CourseOfferInfo(int group, int capacity, int registeredStudents, int gender) {
        this.group = group;
        this.capacity = capacity;
        this.registeredStudents = registeredStudents;
        this.gender = gender;
    }

    public int getGroup() {
        return group;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getRegisteredStudents() {
        return registeredStudents;
    }

    public int getGender() {
        return gender;
    }

    public String getGenderString() {
        if (gender == FOR_MALE )
            return "For Male";
        else if (gender == FOR_FEMALE)
            return "For Female";
        else
            return "For Both (Male and Female)";
    }

    public void incRegStudents() {
        this.registeredStudents++;
    }

    public void decRegStudents() {
        this.registeredStudents--;
    }
}
