package model.course;

import model.professor.Professor;
import model.session.Session;
import model.term.Term;
import repository.specification.course.CourseSqlSpecById;
import repository.specification.term.TermSqlSpecById;
import repository.sql_repo.CourseSqlRepo;
import repository.sql_repo.ProfessorSqlRepo;
import repository.sql_repo.SessionSqlRepo;
import repository.sql_repo.TermSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Sahand on 6/30/2017.
 */
public class CourseOfferFactory {
    private static CourseOfferFactory ourInstance = new CourseOfferFactory();

    public static CourseOfferFactory getInstance() {
        return ourInstance;
    }

    private CourseOfferFactory() {}

    public CourseOffer createWith(int id) {
        return new CourseOffer(id);
    }

    public CourseOffer createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int groupId = resultSet.getInt("group_id");
        int courseId = resultSet.getInt("course_id");
        int professorId = resultSet.getInt("professor_id");
        int termId = resultSet.getInt("term_id");
        int sessionId = resultSet.getInt("session_id");
        int registeredStudents = resultSet.getInt("registered_students");
        int capacity = resultSet.getInt("capacity");
        int gender = resultSet.getInt("gender");

        CourseOfferInfo courseOfferInfo =
                new CourseOfferInfo(groupId, capacity, registeredStudents, gender);
        Course course = CourseSqlRepo
                .getInstance().findCourse(new CourseSqlSpecById(id));
        Term term = TermSqlRepo.getInstance()
                .findTerm(new TermSqlSpecById(termId));
        List<Session> sessions = SessionSqlRepo.getInstance()
                .findSessions(sessionId);
        Professor professor = ProfessorSqlRepo
                .getInstance().findProfessor(professorId);

        return new CourseOffer(id, courseOfferInfo,
                course, term, sessions, professor);
    }
}
