package model.course;

import model.department.Department;

/**
 * Created by Sahand on 5/21/2017.
 */

public class Course {

    private int id;
    private String cid;
    private String name;
    private Department department;
    private int unit;

    Course(String cid, String name, Department department, int unit) {
        this.cid = cid;
        this.name = name;
        this.department = department;
        this.unit = unit;
    }

    Course(int id, String cid,
                  String name, Department department, int unit) {
        this.id = id;
        this.cid = cid;
        this.name = name;
        this.department = department;
        this.unit = unit;
    }

    public int getId() {
        return id;
    }

    public String getCid() {
        return cid;
    }

    public String getName() {
        return name;
    }

    public int getUnit() { return unit; }

    public Department getDepartment() {
        return department;
    }

    public boolean isCourse(Course course) {
        return this.id == course.getId();
    }
}

