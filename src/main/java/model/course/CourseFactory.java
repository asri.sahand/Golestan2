package model.course;

import model.department.Department;
import repository.sql_repo.DepartmentSqlRepo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sahand on 6/30/2017.
 */
public class CourseFactory {
    private static CourseFactory ourInstance = new CourseFactory();

    public static CourseFactory getInstance() {
        return ourInstance;
    }

    private CourseFactory() {}

    public Course createFrom(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String cid = resultSet.getString("cid");
        String name = resultSet.getString("name");
        int did = resultSet.getInt("did");
        int unit = resultSet.getInt("unit");

        Department department = DepartmentSqlRepo
                .getInstance().findDepartment(did);

        return new Course(id, cid, name, department, unit);
    }
}
