package model.course;

import model.professor.Professor;
import model.session.Session;
import model.term.Term;
import repository.sql_repo.CourseOfferSqlRepo;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class CourseOffer {

    private int id;
    private CourseOfferInfo courseOfferInfo;
    private Course course;
    private Term term;
    private List<Session> sessions;
    private Professor professor;


    CourseOffer(int id) {
        this.id = id;
    }

    CourseOffer(int id, CourseOfferInfo courseOfferInfo, Course course,
                       Term term, List<Session> sessions, Professor professor) {
        this.id = id;
        this.courseOfferInfo = courseOfferInfo;
        this.course = course;
        this.term = term;
        this.sessions = sessions;
        this.professor = professor;
    }

    public int getId() {
        return id;
    }

    public CourseOfferInfo getCourseOfferInfo() {
        return courseOfferInfo;
    }

    public Course getCourse() {
        return course;
    }

    public Term getTerm() {
        return term;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void incRegStudents() {
        this.courseOfferInfo.incRegStudents();
        CourseOfferSqlRepo.getInstance().updateCourseOffer(this);
    }

    public void decRegStudents() {
        this.courseOfferInfo.decRegStudents();
        CourseOfferSqlRepo.getInstance().updateCourseOffer(this);
    }

    public boolean isCourse(Course course) {
        return this.getCourse().getId() == course.getId();
    }
}
