package main;

import management.StudentManagement;
import model.course.CourseOffer;
import model.student.Student;
import model.term.Term;
import repository.specification.course_offer.CourseOfferSqlSpecById;
import repository.specification.student.StudentSqlSpecById;
import repository.specification.term.TermSqlSpecNewest;
import repository.sql_repo.CourseOfferSqlRepo;
import repository.sql_repo.StudentSqlRepo;
import repository.sql_repo.TermSqlRepo;
import service.RegistrationService;

import java.util.Scanner;

/**
 * Created by Sahand on 6/5/2017.
 */
public class Main {
    public static void main(String[] args) {
        showHelp();
        RegistrationService registrationService =
                new RegistrationService();

        Student student = StudentSqlRepo.getInstance()
                .findStudent(new StudentSqlSpecById("810192574"));
        Term term = TermSqlRepo.getInstance()
                .findTerm(new TermSqlSpecNewest());

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            String segments[] = input.split(" ");
            if (input.equals("quit") || input.equals("exit")) {
                break;
            } else if (input.equals("show student")) {
                StudentManagement.getInstance().showStudentInfo(student);
            } else if (input.equals("show courses"))  {
                registrationService.showCourseOffers(term);
            } else if (segments[0].equals("enroll") && segments[1].equals("course")) {
                int courseOfferId = Integer.parseInt(segments[2]);
                CourseOffer courseOffer = CourseOfferSqlRepo
                    .getInstance().findCourseOffer(new
                            CourseOfferSqlSpecById(courseOfferId));

                registrationService.enrollCourseOffer(student, term, courseOffer);
            } else if(segments[0].equals("unenroll") && segments[1].equals("course")) {
                int courseOfferId = Integer.parseInt(segments[2]);
                CourseOffer courseOffer = CourseOfferSqlRepo
                        .getInstance().findCourseOffer(new
                                CourseOfferSqlSpecById(courseOfferId));

                StudentManagement.getInstance().reloadStudentTakenCourses(student);
                registrationService.unenrollCourseOffer(student, term, courseOffer);
            }

            showHelp();
        }
    }

    private static void showHelp() {
        System.out.println("Commands: ");
        System.out.println("\tshow courses");
        System.out.println("\tshow student");
        System.out.println("\tenroll course [course_offer_id]");
        System.out.println("\tunenroll course [course_offer_id]");
        System.out.println("\texit");
    }
}
