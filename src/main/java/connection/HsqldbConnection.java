package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Sahand on 5/20/2017.
 */
public class HsqldbConnection {
    private static final String URL =
            "jdbc:hsqldb:hsql://localhost:9001/golestandb";
    private static final String DRIVER =
            "org.hsqldb.jdbcDriver";

//    private static final String JDBC_DRIVER = "org.hsqldb.jdbcDriver";
//    private static final String JDBC_URL = "jdbc:hsqldb:hsql://localhost:9001/xdb";

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}