package service;

import management.CourseOfferManagement;
import management.StudentManagement;
import model.course.CourseOffer;
import model.register.*;
import model.student.Student;
import model.student.StudentTerm;
import model.student.TakenCourseFactory;
import model.term.Term;
import repository.specification.course_offer.CourseOfferSpecByTerm;
import repository.specification.student_term.StudentTermSpecBySidAndTermId;
import repository.specification.taken_course.TakenCourseOfferSpecByCourseOfferId;
import repository.sql_repo.CourseOfferSqlRepo;
import repository.sql_repo.StudentTermSqlRepo;
import repository.sql_repo.TakenCourseSqlRepo;

import java.util.List;

/**
 * Created by Sahand on 6/6/2017.
 */
public class RegistrationService {
    public void showCourseOffers(Term term) {

        List<CourseOffer> courseOffers = CourseOfferSqlRepo.getInstance()
                .findCourseOffers(new CourseOfferSpecByTerm(term));
        for (CourseOffer courseOffer: courseOffers) {
            CourseOfferManagement.getInstance().showCourseOfferInfo(courseOffer);
        }
    }

    public void enrollCourseOffer(Student student, Term term, CourseOffer courseOffer) {
        if (takeCourseCheck(student, courseOffer, term)) {
            StudentTerm studentTerm = StudentTermSqlRepo.getInstance().findStudentTerm(
                    new StudentTermSpecBySidAndTermId(
                            student.getStudentIdentity().getSid(), term.getId()));
            TakenCourseSqlRepo.getInstance().addTakenCourse(
                    TakenCourseFactory.getInstance().createWith(
                            courseOffer), studentTerm);

            StudentManagement.getInstance().reloadStudentTakenCourses(student);
            courseOffer.incRegStudents();
            System.out.println("Course Offer is taken successfully!");
        }
    }

    private boolean takeCourseCheck(
            Student student, CourseOffer courseOffer, Term term) {

        boolean check          = true;
        boolean termCheck      = (new RegTermCheck(courseOffer, term).check());
        boolean capacityCheck  = (new RegCapacityCheck(courseOffer)).check();
        boolean timeConfCheck  = (new RegTimeCheck(student, courseOffer)).check();
        boolean maxUnitCheck   = (new RegMaxUnitCheck(student, term).check());
        boolean courseReqCheck = (new RegReqCheck(student, courseOffer).check());
        if (!termCheck) {
            System.err.println("There is no CourseOffer with such ID in this term!");
            check = false;
        } if (!capacityCheck) {
            System.err.println("Course Offer is full!");
            check = false;
        } if (!timeConfCheck) {
            System.err.println("Time conflict occurred!");
            check = false;
        } if(!maxUnitCheck) {
            System.err.println("You can not take this many units this term!");
            check = false;
        } if (!courseReqCheck) {
            System.err.println("You did not pass requested course(s)!");
            check = false;
        }
        return check;
    }

    public void unenrollCourseOffer(Student student, Term term, CourseOffer courseOffer) {
        TakenCourseSqlRepo.getInstance()
                .removeTakenCourse(new
                        TakenCourseOfferSpecByCourseOfferId(courseOffer.getId()));
        StudentManagement.getInstance().reloadStudentTakenCourses(student);
        courseOffer.decRegStudents();
    }
}
