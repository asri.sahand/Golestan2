import model.department.Department;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahand on 6/7/2017.
 */
public class JavaTest {

    @Test
    public void test1() {
        List<Department> departments1 = new ArrayList<Department>();
        Department department1 = new Department(1, "1");
        Department department2 = new Department(2, "2");
        Department department3 = new Department(3, "3");
        departments1.add(department1);
        departments1.add(department2);
        departments1.add(department3);
        List<Department> departments2 = departments1;
        departments2.remove(0);
        System.out.println(departments1.size());
        System.out.println(departments2.size());
    }

    @Test
    public void test2() {
        List<Department> departments1 = new ArrayList<Department>();
        Department department1 = new Department(1, "1");
        Department department2 = new Department(2, "2");
        Department department3 = new Department(3, "3");
        departments1.add(department1);
        departments1.add(department2);
        departments1.add(department3);
        List<Department> departments2 = new ArrayList<Department>();
        for (Department d: departments1)
            departments2.add(d);
        departments2.remove(0);
        System.out.println(departments1.size());
        System.out.println(departments2.size());
    }
}
